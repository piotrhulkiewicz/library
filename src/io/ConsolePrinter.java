package io;

import model.Book;
import model.Magazine;
import model.Library;
import model.Publication;

public class ConsolePrinter {

    public void printListOfBooksInLibrary(Publication[] publications) {
        int currentNumberOfBooks = 0;
        for (int i = 0; i < publications.length; i++) {
            if (publications[i] instanceof Book) {
                ((Book) publications[i]).printInfo();
                currentNumberOfBooks++;
            }
        }
        if (currentNumberOfBooks == 0) {
            printLine("No books in the library");
        }
    }

    public void printListOfMagazinesInLibrary(Publication[] publications) {
        int currentNumberOfMagazines = 0;
        for (int i = 0; i < publications.length; i++) {
            if (publications[i] instanceof Magazine) {
                ((Magazine) publications[i]).printInfo();
                currentNumberOfMagazines++;
            }
        }
        if (currentNumberOfMagazines == 0) {
            printLine("No magazines in the library");
        }
    }

    public void printLine (String text) {
        System.out.println(text);
    }
}
