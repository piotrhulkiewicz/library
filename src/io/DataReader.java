package io;

import model.Book;
import model.Magazine;

import java.util.Scanner;

public class DataReader {

    private final Scanner scanner = new Scanner(System.in);
    private ConsolePrinter consolePrinter;

    public DataReader(ConsolePrinter consolePrinter) {
        this.consolePrinter = consolePrinter;
    }

    public Book readAndCreateBook() {
        consolePrinter.printLine("Enter book title");
        String title = scanner.nextLine();
        consolePrinter.printLine("Enter author");
        String author = scanner.nextLine();
        consolePrinter.printLine("Enter release year");
        int releaseYear = scanner.nextInt();
        consolePrinter.printLine("Enter number if pages");
        int pages = option();
        consolePrinter.printLine("Enter publisher");
        String publisher = scanner.nextLine();
        consolePrinter.printLine("Enter ISBN");
        String isbn = scanner.nextLine();
        return new Book(title, author, releaseYear, pages, publisher, isbn);
    }

    public Magazine readAndCreateMagazine() {
        consolePrinter.printLine("Enter magazine title");
        String title = scanner.nextLine();
        consolePrinter.printLine("Enter language");
        String language = scanner.nextLine();
        consolePrinter.printLine("Enter release year");
        int releaseYear = option();
        consolePrinter.printLine("Enter release month");
        int releaseMonth = option();
        consolePrinter.printLine("Enter release day");
        int releaseDay = option();
        consolePrinter.printLine("Enter publisher");
        String publisher = scanner.nextLine();
        return new Magazine(title, language, releaseYear, releaseMonth, releaseDay, publisher);
    }

    public void close() {
        scanner.close();
    }

    public int option() {
        try {
            return scanner.nextInt();
        } finally {
            scanner.nextLine();
        }
    }
}
