package model;

public class Publication {

    private String title;
    private String publisher;
    private int releaseYear;

    Publication(String title, int releaseYear, String publisher) {
        this.title = title;
        this.releaseYear = releaseYear;
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }
}
