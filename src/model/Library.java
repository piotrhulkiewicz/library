package model;

public class Library {

    public final int maxNumberOfPublications = 2000;
    public Publication[] publications = new Publication[maxNumberOfPublications];
    public  int currentNumberOfPublications;

    public Publication[] getPublications() {
        Publication[] result = new Publication[currentNumberOfPublications];
        for (int i = 0; i < publications.length; i++) {
            if (publications[i] != null) {
                result[i] = publications[i];
            }
        } return result;
    }

    public void addBookToLibrary(Book book) {
       addPublicationToLibrary(book);
    }

    public void addMagazineToLibrary(Magazine magazine) {
        addPublicationToLibrary(magazine);
    }

    private void addPublicationToLibrary(Publication publication) {
        if (currentNumberOfPublications == maxNumberOfPublications) {
            throw new ArrayIndexOutOfBoundsException("Limit of publications is reached");
        } else {
            publications[currentNumberOfPublications] = publication;
            currentNumberOfPublications++;
        }
    }


}

