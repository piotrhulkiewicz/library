package model;

public class Magazine extends Publication {

    private String language;
    private int releaseMonth;
    private int releaseDay;

    public Magazine(String title, String language, int releaseYear, int releaseMonth, int releaseDay, String publisher) {
        super(title, releaseYear, publisher);
        this.language = language;
        this.releaseMonth = releaseMonth;
        this.releaseDay = releaseDay;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getReleaseMonth() {
        return releaseMonth;
    }

    public void setReleaseMonth(int releaseMonth) {
        this.releaseMonth = releaseMonth;
    }

    public int getReleaseDay() {
        return releaseDay;
    }

    public void setReleaseDay(int releaseDay) {
        this.releaseDay = releaseDay;
    }

    public void printInfo() {
        String info = "Title: " + getTitle() + ", language: " + language + ", Year: " + getReleaseYear() +
                ", Month: " + getReleaseMonth() + ", day " + getReleaseDay() + ", publisher: " + getPublisher();
        System.out.println(info);
    }

}

