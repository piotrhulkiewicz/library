package app;

public class LibraryApp {
    final static String APP_NAME = "Application Library v1.5";
    public static void main(String[] args) {

        LibraryController libraryController = new LibraryController();
        System.out.println(APP_NAME);
        libraryController.libraryControl();
    }
}