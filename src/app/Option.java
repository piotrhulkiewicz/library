package app;

import exceptions.NoSuchOptionException;

import java.util.Arrays;

public enum Option {
    EXIT("Exit program"),
    ADD_BOOK("Add book to the library"),
    ADD_MAGAZINE("Add magazine to the library"),
    PRINT_BOOKS("Print the list of books in the library"),
    PRINT_MAGAZINES("Print the list of magazines in the library");

    private final String description;

    Option(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return ordinal() + " - " + description;
    }

    public static Option optionFromInt(int userChoice) throws NoSuchOptionException {
        try {
            return values()[userChoice];
        } catch (ArrayIndexOutOfBoundsException e)
        { throw new NoSuchOptionException("No option of id: " + userChoice + " Try again.");
        }
    }
}
