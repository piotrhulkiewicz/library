package app;

import exceptions.NoSuchOptionException;
import io.ConsolePrinter;
import io.DataReader;
import model.Library;

import java.util.InputMismatchException;

import static app.Option.*;

class LibraryController {


    private final Library library = new Library();
    private final ConsolePrinter consolePrinter = new ConsolePrinter();
    private final DataReader dataReader = new DataReader(consolePrinter);

//    private final static int EXIT = 0;
//    private final static int ADD_BOOK = 1;
//    private final static int ADD_MAGAZINE = 2;
//    private final static int PRINT_BOOKS = 3;
//    private final static int PRINT_MAGAZINES = 4;


    public void libraryControl() {
        Option option;
        do {
            printOptions();
            option = getOption();
            switch (option) {
                case EXIT -> exitProgram();
                case ADD_BOOK -> addBook();
                case ADD_MAGAZINE -> addMagazine();
                case PRINT_BOOKS -> printListOfBooks();
                case PRINT_MAGAZINES -> printListOfMagazines();
            }
        } while (option != Option.EXIT);
    }

    private Option getOption() {
        boolean optionOK = false;
        Option option = null;
        while (!optionOK) {
            try {
                option = Option.optionFromInt(dataReader.option());
                optionOK = true;
            } catch (NoSuchOptionException e) {
                consolePrinter.printLine(e.getMessage());
            } catch (InputMismatchException e) {
                consolePrinter.printLine("Entered value which is not a number. Try again");
            }
        }
        return option;
    }

    private void printListOfMagazines() {
        consolePrinter.printListOfMagazinesInLibrary(library.getPublications());
    }

    private void addMagazine() {
        try {
            library.addMagazineToLibrary(dataReader.readAndCreateMagazine());
        } catch (InputMismatchException e) {
            consolePrinter.printLine("No possible to create and add magazine, wrong data.");
        } catch (ArrayIndexOutOfBoundsException e) {
            consolePrinter.printLine("No place in the library.");
        }
    }

        private void printOptions () {
            consolePrinter.printLine("Select an option");
            for (Option option : values()) {
                consolePrinter.printLine(option.toString());
            }

        }

        private void printListOfBooks () {
            consolePrinter.printListOfBooksInLibrary(library.getPublications());
        }

        private void addBook () {
            try {
                library.addBookToLibrary(dataReader.readAndCreateBook());
            } catch (InputMismatchException e) {
                consolePrinter.printLine("No possible to create and add book, wrong data.");
            } catch (ArrayIndexOutOfBoundsException e) {
                consolePrinter.printLine("No place in the library.");
            }
        }

        private void exitProgram () {
            consolePrinter.printLine("End of the program. Goodbye");
            dataReader.close();
        }
    }
